package se.alten.montyhallproblem;

import com.vaadin.flow.spring.annotation.EnableVaadin;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableVaadin
public class MontyHallProblemApplication {

	public static void main(String[] args) {
		SpringApplication.run(MontyHallProblemApplication.class, args);
	}

}
