package se.alten.montyhallproblem.ui;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import se.alten.montyhallproblem.domain.Box;
import se.alten.montyhallproblem.domain.Statistics;
import se.alten.montyhallproblem.service.BoxService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-13
 */
@Route("main")
public class MainView extends VerticalLayout {

    private Button box1 = new Button("Box 1");
    private Button box2 = new Button("Box 2");
    private Button box3 = new Button("Box 3");
    private List<Button> buttons = new ArrayList<>();
    private int click = 0;
    private List<Box> boxes;
    private List<Statistics> statisticsList = new ArrayList<>();
    private Button startGame = new Button("Start game");
    private Button statisticsButton = new Button("Show statistics");
    private boolean decisionChanged = false;


    @Autowired
    private BoxService boxService;

    MainView(BoxService boxService) {
        this.boxService = boxService;
        generateValues();
        add(createButtons());
        startGame.addClickListener(clickEvent -> generateValues());

    }


    private FormLayout createButtons() {
        FormLayout formLayout = new FormLayout(new Label("Monty Hall Problem"));
        formLayout.addFormItem(startGame, "");

        formLayout.add(new Label("One of the boxes has money. Select the box"));

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        buttons.forEach(this::addClickListenersToBoxes);
        addStatisticsListener();
        horizontalLayout.add(box1, box2, box3);

        formLayout.addFormItem(new Text(""), "");
        formLayout.add(horizontalLayout);

        formLayout.addFormItem(statisticsButton, "");
        return formLayout;
    }

    private void addStatisticsListener() {
        statisticsButton.addClickListener(clickEvent -> {
            Dialog dialog = new Dialog(new Label("Statistics"));
            FormLayout formLayout = new FormLayout();

            formLayout.addFormItem(new Text(String.valueOf((int) statisticsList.stream()
                    .filter(x -> x.isWin() && x.isDecisionChanged()).count())), "Wins on decision changed");
            formLayout.addFormItem(new Text(String.valueOf((int) statisticsList.stream()
                    .filter(x -> !x.isWin() && x.isDecisionChanged()).count())), "Lost on decision changed");

            formLayout.addFormItem(new Text(String.valueOf(statisticsList.stream()
                    .filter(x -> x.isWin() && !x.isDecisionChanged()).count())), "Wins on decision not changed");
            formLayout.addFormItem(new Text(String.valueOf(statisticsList.stream()
                    .filter(x -> !x.isWin() && !x.isDecisionChanged()).count())), "Losts on decision not changed");
            formLayout.addFormItem(new Text(String.valueOf(statisticsList.size())), "Total games played");

            dialog.add(formLayout);
            dialog.open();
        });

    }

    private void spreadBoxedBetweenButtons() {
        boxes.stream().findAny().ifPresent(x -> x.setButton(box1));
        boxes.stream().filter(x -> x.getButton() == null).findAny().ifPresent(x -> x.setButton(box2));
        boxes.stream().filter(x -> x.getButton() == null).findAny().ifPresent(x -> x.setButton(box3));
    }

    private void generateValues() {
        buttons.add(box1);
        buttons.add(box2);
        buttons.add(box3);

        click = 0;
        boxes = boxService.generateBoxes();

        spreadBoxedBetweenButtons();

        buttons.forEach(x -> x.setEnabled(true));
        decisionChanged = false;

    }

    private void addClickListenersToBoxes(Button button) {
        button.addClickListener(buttonClickEvent -> boxes.stream().filter(x -> x.getButton().getText().equals(button.getText())).findFirst().ifPresent(b -> {
            click++;
            b.setSelected(true);
            if (click == 2) {
                Statistics statistics = new Statistics();
                if (b.isHasMoney()) {
                    Notification.show("You win the money! " + b.getButton().getText() + " has the money!", 2000, Notification.Position.MIDDLE);
                    statistics.setWin(true);
                    statistics.setDecisionChanged(decisionChanged);
                    statisticsList.add(statistics);

                } else {
                    Notification.show(b.getButton().getText() + " Has no money! Good luck next time!", 2000, Notification.Position.MIDDLE);
                    statistics.setWin(false);
                    statistics.setDecisionChanged(decisionChanged);
                    statisticsList.add(statistics);

                }
                startGame.click();
            } else {
                boxes.stream().filter(box -> !box.isHasMoney() && !box.isSelected()).findFirst()
                        .ifPresent(x -> {
                            x.getButton().setEnabled(false);
                            com.vaadin.flow.component.dialog.Dialog choice = changeChoiceDialog(b, x);
                            choice.open();
                        });

            }
        }));
    }

    private Dialog changeChoiceDialog(Box selectedButton, Box emptyButton) {
        StringBuilder text = new StringBuilder().append("You selected ").append(selectedButton.getButton().getText())
                .append(". We opened ").append(emptyButton.getButton().getText()).append(" and it has no money. Would you like to change your box?");

        FormLayout formLayout = new FormLayout();
        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");

        com.vaadin.flow.component.dialog.Dialog dialog = new com.vaadin.flow.component.dialog.Dialog(new Label(text.toString()));


        yesButton.addClickListener(buttonClickEvent -> {
            dialog.close();
            selectedButton.setSelected(false);
            emptyButton.getButton().setEnabled(false);
            boxes.stream().filter(x -> x != selectedButton && x != emptyButton).findFirst().ifPresent(t -> t.getButton().click());
            decisionChanged = true;
        });

        noButton.addClickListener(buttonClickEvent -> {
            dialog.close();
            selectedButton.getButton().click();
        });

        formLayout.add(yesButton, noButton);
        dialog.add(formLayout);

        return dialog;

    }


}
