package se.alten.montyhallproblem.domain;

import lombok.Data;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-13
 */
@Data
public class Statistics {
    private boolean decisionChanged;
    private boolean isWin;
}
