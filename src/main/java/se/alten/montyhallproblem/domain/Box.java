package se.alten.montyhallproblem.domain;

import com.vaadin.flow.component.button.Button;
import lombok.Data;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-13
 */
@Data
public class Box {

    private boolean hasMoney;
    private Button button;
    private boolean selected;

}
