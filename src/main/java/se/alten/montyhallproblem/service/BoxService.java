package se.alten.montyhallproblem.service;

import org.springframework.stereotype.Service;
import se.alten.montyhallproblem.domain.Box;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-13
 */
@Service
public class BoxService {

    public List<Box> generateBoxes(){

        List<Box> boxes = new LinkedList<>();

        Box box1 = new Box();
        box1.setHasMoney(true);
        box1.setButton(null);
        boxes.add(box1);

        Box box2 = new Box();
        box2.setHasMoney(false);
        box2.setButton(null);
        boxes.add(box2);

        Box box3 = new Box();
        box3.setHasMoney(false);
        box3.setButton(null);
        boxes.add(box3);

        Collections.shuffle(boxes);

        return boxes;
    }


}
